# OpenML dataset: breast

https://www.openml.org/d/45632

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

See [https://github.com/slds-lmu/paper_2023_ci_for_ge](https://github.com/slds-lmu/paper_2023_ci_for_ge) for a description.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45632) of an [OpenML dataset](https://www.openml.org/d/45632). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45632/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45632/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45632/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

